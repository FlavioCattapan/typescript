// Exercício 1 - Classe
// function Moto(nome) {
//     this.nome = nome
//     this.velocidade = 0
 
//     this.buzinar = function() {
//         console.log('Toooooooooot!')
//     }
 
//     this.acelerar= function(delta) {
//         this.velocidade = this.velocidade + delta
//     }
// }
 
// var moto = new Moto('Ducati')
// moto.buzinar()
// console.log(moto.velocidade)
// moto.acelerar(30)
// console.log(moto.velocidade)
// Resposta Exercício 1 - Classe
class Moto {
    constructor(public nome:string, public velocidade:number = 0){}
     buzinar () {
        console.log('Toooooooooot!')
    }
    acelerar (delta: number) {
        this.velocidade = this.velocidade + delta
    }
}
 
const moto = new Moto('Ducati', 10)
moto.buzinar()
console.log(moto.velocidade)
moto.acelerar(30)
console.log(moto.velocidade)
 
// Exercício 2 - Herança
abstract class Objeto2D {
    constructor(public base:number,public altura:number){}
    abstract calculaArea():number
}

class Retangulo extends Objeto2D {
    public calculaArea():number{
        return this.altura * this.base;
    }
}
const objeto2D = new Retangulo(10,10);
objeto2D.calculaArea();
console.log(objeto2D.calculaArea())
 
// Exercício 3 - Getters & Setters
class Estagiario  {
    private _primeiroNome: string;

    get primeiroNome(){
        return this._primeiroNome
    }

    set primeiroNome(nome: string){
        this._primeiroNome = nome;
    }

}

const estagiario = new Estagiario
console.log(estagiario.primeiroNome)
estagiario.primeiroNome = 'Le'
console.log(estagiario.primeiroNome)
estagiario.primeiroNome = 'Leonardo'
console.log(estagiario.primeiroNome)