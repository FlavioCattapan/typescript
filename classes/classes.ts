class Data {
    // publico por padrao
    public dia: number
    mes: number
    ano: number
    // não tem sobrecarga de metodos
    constructor(dia: number = 1, mes: number = 12, ano: number = 1970) {
        this.dia = dia
        this.mes = mes
        this.ano = ano
    }

}

const aniversario = new Data(3, 11, 1991)
aniversario.dia = 4
console.log(aniversario.dia)
console.log(aniversario.mes)

// posso emitir os parenteses
const casamento = new Data
console.log(casamento.mes)

// não precisa declarar os parametros
class DataEsperta {
    constructor(public dia: number = 1, public mes: number = 12, public ano: number = 1970) {
    }
}

class Produto {
    constructor(public nome: string, public preco: number, public desconto: number = 0) {
    }

    // a palavra public  e opcional
    public resumo(): string {
        return `${this.nome} custa R$ ${this.precoComDesconto()} (${this.desconto * 100}% off)`
    }

    precoComDesconto(): number {
        return this.preco - this.preco * this.desconto;
    }

}

const produto1 = new Produto('Batata 1', 10);
produto1.desconto = 0.06
console.log(produto1)
console.log(produto1.resumo())
const produto2 = new Produto('Batata 2 ', 10, 0.80);
console.log(produto2)
console.log(produto2.resumo())

// modificadores de acesso não existe no javascript

class Carro {
    private velocidadeAtual: number = 0;

    constructor(public marca: string, public modelo: string, private velocidadeMaxima = 200) {
    }

    protected alterarVelocidade(delta: number): number {
        const novaVelocidade = this.velocidadeAtual + delta
        const velocidadeValida = novaVelocidade >= 0 && novaVelocidade <= this.velocidadeMaxima
        if (velocidadeValida) {
            this.velocidadeAtual = novaVelocidade;
        } else {
            this.velocidadeAtual = delta > 0 ? this.velocidadeMaxima : 0;
        }
        return this.velocidadeAtual;
    }

    public acelerar(): number {
        return this.alterarVelocidade(5);
    }

    public frear(): number {
        return this.alterarVelocidade(-5);
    }

}

const carro1 = new Carro('Ford', 'Ka', 185)
console.log(carro1.acelerar())
console.log(carro1.acelerar())
console.log(carro1.acelerar())

Array(50).fill(0).forEach(element => {
    console.log(carro1.acelerar());
});

Array(35).fill(0).forEach(element => {
    console.log(carro1.frear());
});

class Ferrari extends Carro {

    constructor(modelo: string, velocidadeMaxima: number) {
        super('Ferrari', modelo, velocidadeMaxima);
    }

    public acelerar(): number {
        return this.alterarVelocidade(20);
    }

    public frear(): number {
        return this.alterarVelocidade(-20);
    }

}

const f4 = new Ferrari('F40', 324);
console.log(`${f4.marca} ${f4.modelo}`)
console.log(f4.acelerar());
console.log(f4.frear());

// getters and setters
class Pessoa {

    private _idade: number = 0;

    // quando precisa de uma validação
    get idade(): number {
        return this._idade;
    }

    set idade(valor: number) {
        if (valor >= 0 && valor <= 120) {
            this._idade = valor
        }
    }

}

const pessoa1 = new Pessoa()
// chama o metodo set
pessoa1.idade = 10
// chama o metodo get
console.log(pessoa1.idade)

pessoa1.idade = -3;
console.log(pessoa1.idade)

// atributos e metodos estaticos
class Matematica {
    static PI: number = 3.1416

    static areaCirc(raio: number): number {
        return this.PI * raio * raio;
    }
}

const m1 = new Matematica();
console.log(Matematica.areaCirc(4))

abstract class X {
    // tem que colocar o nome do metodo como abstrato
    abstract y(a: number): number
    w(b: number): number {
        return 10;
    }

}


abstract class Calculo {

    protected resultado: number = 0;

    abstract executar(...numeros: number[]): void

    getResultado(): number {
        return this.resultado
    }

}

class Soma extends Calculo {
    executar(...numeros: number[]): void {
        this.resultado = numeros.reduce((t, a) => t + a);
    }
}

class Multiplicacao extends Calculo {
    executar(...numeros: number[]): void {
        this.resultado = numeros.reduce((t, a) => t * a);
    }
}

let c1 = new Soma();
c1.executar(2, 3, 4);
console.log(c1.getResultado)

c1 = new Multiplicacao();
c1.executar(2, 3, 4);
console.log(c1.getResultado)

class Unico {
    private static instance: Unico = new Unico

    private constructor() { }

    static getInstance() {
        return Unico.instance
    }

    agora() {
        return new Date
    }

    static agoraS() {
        return new Date
    }

}

//const errado = new Unico()
console.log(Unico.getInstance().agora())
console.log(Unico.agoraS())

// somente leitura
class Aviao {
    public readonly modelo: string
    constructor(modelo: string, public readonly prefixo: string) {
        this.modelo = modelo
    }
}

const av1 = new Aviao('Mod1', 'Pref1');
//av1.modelo = '200';
