"use strict";
class Data {
    // não tem sobrecarga de metodos
    constructor(dia = 1, mes = 12, ano = 1970) {
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
    }
}
const aniversario = new Data(3, 11, 1991);
aniversario.dia = 4;
console.log(aniversario.dia);
console.log(aniversario.mes);
// posso emitir os parenteses
const casamento = new Data;
console.log(casamento.mes);
// não precisa declarar os parametros
class DataEsperta {
    constructor(dia = 1, mes = 12, ano = 1970) {
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
    }
}
class Produto {
    constructor(nome, preco, desconto = 0) {
        this.nome = nome;
        this.preco = preco;
        this.desconto = desconto;
    }
    // a palavra public  e opcional
    resumo() {
        return `${this.nome} custa R$ ${this.precoComDesconto()} (${this.desconto * 100}% off)`;
    }
    precoComDesconto() {
        return this.preco - this.preco * this.desconto;
    }
}
const produto1 = new Produto('Batata 1', 10);
produto1.desconto = 0.06;
console.log(produto1);
console.log(produto1.resumo());
const produto2 = new Produto('Batata 2 ', 10, 0.80);
console.log(produto2);
console.log(produto2.resumo());
// modificadores de acesso não existe no javascript
class Carro {
    constructor(marca, modelo, velocidadeMaxima = 200) {
        this.marca = marca;
        this.modelo = modelo;
        this.velocidadeMaxima = velocidadeMaxima;
        this.velocidadeAtual = 0;
    }
    alterarVelocidade(delta) {
        const novaVelocidade = this.velocidadeAtual + delta;
        const velocidadeValida = novaVelocidade >= 0 && novaVelocidade <= this.velocidadeMaxima;
        if (velocidadeValida) {
            this.velocidadeAtual = novaVelocidade;
        }
        else {
            this.velocidadeAtual = delta > 0 ? this.velocidadeMaxima : 0;
        }
        return this.velocidadeAtual;
    }
    acelerar() {
        return this.alterarVelocidade(5);
    }
    frear() {
        return this.alterarVelocidade(-5);
    }
}
const carro1 = new Carro('Ford', 'Ka', 185);
console.log(carro1.acelerar());
console.log(carro1.acelerar());
console.log(carro1.acelerar());
Array(50).fill(0).forEach(element => {
    console.log(carro1.acelerar());
});
Array(35).fill(0).forEach(element => {
    console.log(carro1.frear());
});
class Ferrari extends Carro {
    constructor(modelo, velocidadeMaxima) {
        super('Ferrari', modelo, velocidadeMaxima);
    }
    acelerar() {
        return this.alterarVelocidade(20);
    }
    frear() {
        return this.alterarVelocidade(-20);
    }
}
const f4 = new Ferrari('F40', 324);
console.log(`${f4.marca} ${f4.modelo}`);
console.log(f4.acelerar());
console.log(f4.frear());
// getters and setters
class Pessoa {
    constructor() {
        this._idade = 0;
    }
    // quando precisa de uma validação
    get idade() {
        return this._idade;
    }
    set idade(valor) {
        if (valor >= 0 && valor <= 120) {
            this._idade = valor;
        }
    }
}
const pessoa1 = new Pessoa();
// chama o metodo set
pessoa1.idade = 10;
// chama o metodo get
console.log(pessoa1.idade);
pessoa1.idade = -3;
console.log(pessoa1.idade);
// atributos e metodos estaticos
class Matematica {
    static areaCirc(raio) {
        return this.PI * raio * raio;
    }
}
Matematica.PI = 3.1416;
const m1 = new Matematica();
console.log(Matematica.areaCirc(4));
class X {
    w(b) {
        return 10;
    }
}
class Calculo {
    constructor() {
        this.resultado = 0;
    }
    getResultado() {
        return this.resultado;
    }
}
class Soma extends Calculo {
    executar(...numeros) {
        this.resultado = numeros.reduce((t, a) => t + a);
    }
}
class Multiplicacao extends Calculo {
    executar(...numeros) {
        this.resultado = numeros.reduce((t, a) => t * a);
    }
}
let c1 = new Soma();
c1.executar(2, 3, 4);
console.log(c1.getResultado);
c1 = new Multiplicacao();
c1.executar(2, 3, 4);
console.log(c1.getResultado);
class Unico {
    constructor() { }
    static getInstance() {
        return Unico.instance;
    }
    agora() {
        return new Date;
    }
    static agoraS() {
        return new Date;
    }
}
Unico.instance = new Unico;
//const errado = new Unico()
console.log(Unico.getInstance().agora());
console.log(Unico.agoraS());
// somente leitura
class Aviao {
    constructor(modelo, prefixo) {
        this.prefixo = prefixo;
        this.modelo = modelo;
    }
}
const av1 = new Aviao('Mod1', 'Pref1');
//av1.modelo = '200';
//# sourceMappingURL=classes.js.map