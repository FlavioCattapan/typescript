// idade é um atributo opcional
// prop tem um nome dinamico
interface Humano {
    nome: string,
    idade?: number
    [prop: string]: any,
    saudar(sobrenome: string): void
}
// exige que pessoa tenha um atributo nome
// function saudarComOla(pessoa: {nome:string}){
// console.log('Ola, ' +pessoa.nome)
// }
function saudarComOla(pessoa: Humano){
    console.log('Ola, ' +pessoa.nome)
}
// function mudarNome(pessoa: {nome: string} ){
//     pessoa.nome = 'Joana';
// }
function mudarNome(pessoa: Humano ){
    pessoa.nome = 'Joana';
}

const pessoa: Humano = {
    nome: 'João',
    idade: 27,
    ["abc"]: true,
    saudar(sobrenome: string){
        console.log('Ola, meu nome é '+
        this.nome + ' '+ sobrenome)
    }
}

console.log(saudarComOla(pessoa))
mudarNome(pessoa)
console.log(saudarComOla(pessoa))
console.log(pessoa.abc)

// usando no contexto de classe

class Cliente implements Humano {
    nome: string = '';
    saudar(sobrenome: string){

    }
}

const meuCliente = new Cliente()
meuCliente.nome = 'Han'

// interface Função
interface FuncaoCalculo {
    (a: number, b: number): number
}

let potencia: FuncaoCalculo

potencia = function (a: number, b: number): number {
   return Math.pow(a,b);
}

console.log(3,10);

// Herança
interface A {
    a(): void
}

interface B {
    b(): void
}

interface ABC extends A, B {
    c(): void;
}
class RealA implements A {
    a(): void {}
}

class RealAB implements A, B {
    a(): void {}
    b(): void {}
}

// obrigado implementar os metodos
abstract class AbstractABD implements A,B {
    a(): void {}
    b(): void {}
    abstract d(): void
}

interface Object {
    log(): void
}

Object.prototype.log = function(){
    console.log(this.toString())
}

const xL = 2
xL.log();

const cli = {
    nome: 'Pedro',
    toString(){
        return this.nome;
    }
}

cli.log();