"use strict";
// exige que pessoa tenha um atributo nome
// function saudarComOla(pessoa: {nome:string}){
// console.log('Ola, ' +pessoa.nome)
// }
function saudarComOla(pessoa) {
    console.log('Ola, ' + pessoa.nome);
}
// function mudarNome(pessoa: {nome: string} ){
//     pessoa.nome = 'Joana';
// }
function mudarNome(pessoa) {
    pessoa.nome = 'Joana';
}
const pessoa = {
    nome: 'João',
    idade: 27,
    ["abc"]: true,
    saudar(sobrenome) {
        console.log('Ola, meu nome é ' +
            this.nome + ' ' + sobrenome);
    }
};
console.log(saudarComOla(pessoa));
mudarNome(pessoa);
console.log(saudarComOla(pessoa));
console.log(pessoa.abc);
// usando no contexto de classe
class Cliente {
    constructor() {
        this.nome = '';
    }
    saudar(sobrenome) {
    }
}
const meuCliente = new Cliente();
meuCliente.nome = 'Han';
let potencia;
potencia = function (a, b) {
    return Math.pow(a, b);
};
console.log(3, 10);
class RealA {
    a() { }
}
class RealAB {
    a() { }
    b() { }
}
// obrigado implementar os metodos
class AbstractABD {
    a() { }
    b() { }
}
Object.prototype.log = function () {
    console.log(this.toString());
};
const xL = 2;
xL.log();
const cli = {
    nome: 'Pedro',
    toString() {
        return this.nome;
    }
};
cli.log();
//# sourceMappingURL=interface.js.map