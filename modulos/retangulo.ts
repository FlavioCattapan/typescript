export default function areaRetangulo(base: number
    , altura: number) {
        return base * altura;
}