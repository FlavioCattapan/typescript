"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const PI = 3.34;
function areaCircunferencia(raio) {
    return raio * raio * PI;
}
exports.areaCircunferencia = areaCircunferencia;
//# sourceMappingURL=circunferencia.js.map