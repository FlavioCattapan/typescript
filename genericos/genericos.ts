import { type } from "os";
import { inflateSync } from "zlib";

// aceita todos os tipos
function echo(objeto: any) {
    return objeto;
}

console.log(echo('João').length)
console.log(echo(27))
console.log(echo({ nome: 'João', idade: 27 }))

// Usando generics
function echoMelhorado<T>(objeto: T) : T {
    return objeto;
}
// tipo declarado na execução porque passou uma string
console.log(echoMelhorado('João').length)
// substitui todos o T por number
console.log(echoMelhorado<number>(27))
// o tipo retorna um objeto ai eu posso colocar o .nome
console.log(echoMelhorado({ nome: 'João', idade: 27 }).nome)

// Generics
const avaliacoes: Array<number> = [10,9.3,7.7]
avaliacoes.push(8.4)
// avaliacoes.push('5.5')
console.log(avaliacoes)

// Array
function imprimir<T>(args: T[]){
    args.forEach(elemento => console.log(elemento))
}

imprimir([1,2,3])
imprimir<number>([1,2,3])
imprimir<string>(['Ana', 'João'])

type Aluno = {nome: string, idade: number}

imprimir<Aluno>([{nome: 'F', idade: 1},{nome: 'J', idade: 2}]) 

// Tipo Generico
// retorna um T => T
type Echo = <T>(data: T) => T
// metodo do tipo Echo recebe um echoMelhorado
const chamarEcho: Echo = echoMelhorado
console.log(chamarEcho<string>('Alguma Coisa'))

// Classe com Generics
abstract class OperacaoBinaria<T,R>{
    constructor(public operando1: T, public operando2: T){}
    abstract executar(): R
}

class SomaBinaria extends OperacaoBinaria<number,number> {

    executar(): number {
        return this.operando1 + this.operando2
    }

}
// não precisa informar os tipos foi resolvido na criação da classe
console.log(new SomaBinaria(3,4));

class DiferencaEntreDatas extends OperacaoBinaria<Data,String> {

    executar(): string {
        const t1 = this.getTime(this.operando1)
        const t2 = this.getTime(this.operando2)
        const diferenca = Math.abs(t1 - t2)
        const dia = 1000 * 60 * 60 * 24
        return `Diferanca + ${Math.ceil(diferenca / dia)}`;

    }

    getTime(data: Data): number {
        let {dia, mes, ano} = data;
        return new Date(`${mes}/${dia}/${ano}`).getTime()
    }

}

const d1 = new Data(1,2, 2020);
const d2 = new Data(10,2,2020)
console.log(new DiferencaEntreDatas(d1,d2).executar())

// Desafio Class Fila
// Atributo: fila (Array)
// Metodos: entrar, proximo, imprimir
// O T só pode ser do tipo number ou do tipo string
class Fila<T extends number | string> {
    private fila: Array<T>
    constructor(...args: T[]){
        this.fila = args;
    }
    entrar(elemento: T){
        this.fila.push(elemento)
    }
    proximo(): T {
        const primeiro = this.fila[0]
        // deleta o item 0 na quantidade 1
        this.fila.splice(0,1)
        return primeiro
    }

    imprimir(){
        console.log(this.fila)
    }
}

const fila = new Fila<string>('Gui', 'Pedro', 'Ana')
fila.imprimir();
fila.entrar('Rafael');
fila.imprimir();
fila.proximo();
fila.proximo();
fila.imprimir();

const fila2 = new Fila<number>(2, 3, 4);

// Desafio Mapa
// Array de Objetos (Chave/Valor) -> itens
// Métodos: obter(Chave), colocar({ C, V })
// limpar(), imprimir()

type Par<C, V> = {
    chave: C,
    valor: V
}

class Mapa<C,V> {
    private itens: Array<Par<C,V>>
    constructor(){
        this.itens = new Array<Par<C,V>>();
    }
    colocar(par: Par<C,V>){
        const encontrado = this.obter(par.chave);
        if(encontrado){
            encontrado.valor = par.valor;
        }else{
            this.itens.push(par);
        }
    }

    limpar(){
        this.itens = new Array<Par<C,V>>();
    }

    imprimir(){
        this.itens.forEach(i => {
            let {chave, valor} = i;
            console.log(`Chave ${chave} - Valor ${valor}`)
        })
    }

    obter(chave: C): Par<C,V> | null{
        const resultado =
        this.itens.filter(i => i.chave === chave);
        return resultado ? resultado[0] : null;
    }

}
 
const mapa = new Mapa<number, string>()
mapa.colocar({ chave: 1, valor: 'Pedro' })
mapa.colocar({ chave: 2, valor: 'Rebeca' })
mapa.colocar({ chave: 3, valor: 'Maria' })
mapa.colocar({ chave: 1, valor: 'Gustavo' })
 
console.log(mapa.obter(2))
mapa.imprimir()
mapa.limpar()
mapa.imprimir()