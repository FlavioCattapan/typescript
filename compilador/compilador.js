"use strict";
let canal = 'Gaveta';
let inscritos = 310234;
console.log(`Canal = ${canal}`);
// não compila com erro  "noEmitOnError": true, no arquivo tsconfig.json
//nome: string = "Pedro M"; está em outro arquivo
nome = "Compila";
console.log(`Nome : ${nome} `);
// função com o nome dentro
(function () {
    //  let nome: string = "ana";
})();
// não coloca o any "noImplicitAny": true, 
//function soma(a, b){
//  return a + b;
//}
// analiza o fluxo
let quarquerCoisa;
quarquerCoisa = 12;
quarquerCoisa = 'teste';
// valida o fluxo"strictNullChecks": false, /* valida se a variavel foi inicializada antes de usar  */ 
function saudar(isManha, horas) {
    let saudacao;
    if (isManha) {
        saudacao = 'Bom dia';
    }
    else {
        saudacao = 'Tenha um bom dia';
    }
    return saudacao;
}
//# sourceMappingURL=compilador.js.map