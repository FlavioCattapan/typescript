"use strict";
var Geometria;
(function (Geometria) {
    let Area;
    (function (Area) {
        const PI = 3.14;
        function areaCicuferencia(raio) {
            return PI * Math.pow(raio, 2);
        }
        Area.areaCicuferencia = areaCicuferencia;
    })(Area = Geometria.Area || (Geometria.Area = {}));
})(Geometria || (Geometria = {}));
//# sourceMappingURL=geometricaCirc.js.map