"use strict";
var Geometria;
(function (Geometria) {
    let Area;
    (function (Area) {
        function areaRestangulo(base, altura) {
            return base * altura;
        }
        Area.areaRestangulo = areaRestangulo;
    })(Area = Geometria.Area || (Geometria.Area = {}));
})(Geometria || (Geometria = {}));
//# sourceMappingURL=geometriaRet.js.map