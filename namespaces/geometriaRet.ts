namespace Geometria {
    export namespace Area {

        export function areaRestangulo(base: number, altura: number) {
            return base * altura
        }

    }
}