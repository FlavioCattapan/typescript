const path = require('path')

module.exports = {
    mode: 'production',
    entry: './modulos/modulos.js',
    output: {
        filename: 'app.min.js',
        path: path.join(_dirname, 'dist')
    },
    resolve:{
        extensions: ['.ts', '.js']
    },
    module: {
        rules: [{
            test: /\.ts$/,
            use: 'ts-loader',
            exclude: /node_modules/
        }]
    }
}

