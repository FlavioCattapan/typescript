"use strict"
var nome: string = 'João'
console.log('nome')

let idade = 27
idade = 27.5
console.log(idade)

let possuiHobbies: Boolean = false
//possuiHobbies = 1;
console.log(possuiHobbies)

let minhaIdade = 'idade 27'
//minhaIdade = 23
console.log(typeof minhaIdade)

let hobbies:any[] = ["Conzinhar", "Esportes"];
console.log(hobbies[0]);
console.log(typeof hobbies)

hobbies = [100, 200, 300];

//tuplas
let endereco: [string, number, number] = ["Avenida Principal", 90, 123];
console.log(endereco);

// enums
enum Cor {
    Cinza,
    Verde = 100,
    Azul = 10,
    Laranja,
    Amarelo,
    Vermelho = 100
}

let minhaCor: Cor = Cor.Verde;
console.log(minhaCor);
console.log(Cor.Azul);

//any
let carro: any = "BMW";
console.log(carro);
carro = {marca: "BMW", ano: 1976};

function retornaMeuNome():string{
    return nome
}
console.log(retornaMeuNome())

function digaOi(): void {
    console.log('Oi')
}

digaOi()

function multiplicar(numA: number, numB: number): number {
    return numA * numB
}

console.log(multiplicar(10, 8));

const teste = function teste(numA: number, numB: number){
    return numA + numB;
}

let calculo
calculo = digaOi;
calculo();
calculo = multiplicar
console.log(calculo(5,6))

let funcao3: (numeroA: number, numeroB: number) => number;
funcao3 = multiplicar
console.log(calculo(5,6))

let usuario = {
    nome: 'João',
    idade: 27
}

let usuario2: {nome: string, idade:number} = {
    nome: 'João',
    idade: 27
}
//usuario = {
 //   name : 'Maria',
 //   age: 31
//}

type Funcionario = {
    supervisores: string[],
    baterPonto: (horas: number) => string
}

let funcionario: Funcionario = {
     supervisores: ['Ana', 'Fernando'],
     baterPonto(ponto: number): string{
     if(ponto <= 8){
         return 'Ponto Normal';
     }else{
        return 'Fora do horário';
     }
 }
}

let funcionario2: Funcionario = {
     supervisores: ['Ana', 'Fernando'],
     baterPonto(ponto: number): string{
     if(ponto <= 8){
         return 'Ponto Normal';
     }else{
        return 'Fora do horário';
     }
 }
}

console.log(funcionario.supervisores)
console.log(funcionario.baterPonto(1))
console.log(funcionario.baterPonto(10))

// Unios Types

let nota: number | string = 10
console.log(`Minha nota é ${nota}`)
nota = '10';
console.log(nota)
nota = 'nota  legal'
console.log(nota)

// chacando tipo forma manual
let valor = 30
if(typeof valor === "number"){
    console.log('É um ')
}

// never ela nunca retorn
function falha(msg : string):never{
    throw new Error(msg);
    //while(true){}
}

const produto = {
    nome: 'Sabao',
    preco: -1,
    validarProduto(){
        if(!this.nome || this.nome.trim().length == 0){
            falha('Precisa ter um nome')
        }
        if(this.preco <= 0){
            falha('Preco invalido')
        }
    }
}

// comentado para funcionar o resto
//produto.validarProduto();

//precisa ter um valor valido
let altura = 12
//altura = null

let alturaOpicional: null | number = 12
alturaOpicional = null

type  Contato = {
   nome: string,
   tel1: string,
   tel2: string | null
}

const contato1: Contato = {
    nome: 'Fulano',
    tel1: '987655432',
    tel2: null
}

console.log(contato1.nome)
console.log(contato1.tel1)
console.log(contato1.tel2)

// vira o tipo any
let podeSerNulo = null
podeSerNulo = 12
console.log(podeSerNulo)

// desafio tipar as classes abaixo
//let contaBancariaSemTipo = {
  //  saldo: 34560,
   // depositar(valor) {
     //   this.saldo += valor;
    //}
//}
// conta bancaria tipada
type ContaBancaria = {
    saldo: number,
    depositar: (valor: number) => void
}
// conta bancaria sem tipo
// let correntistaSemTipo = {
//     nome: 'Ana Silva',
//     contaBancaria: contaBancariaSemTipo,
//     contatos: ['33213213','321321321']
// }

type Correntista = {
    nome: string,
    contaBancaria :  ContaBancaria,
    contatos: string[]
}

let contaBancaria: ContaBancaria  = {
    saldo: 3588,
    depositar(valor: number) {
         this.saldo += valor;
    }
}

let correntista: Correntista  = {
    nome: 'João da Coves',
    contaBancaria: contaBancaria,
    contatos: ['321321','321321321']
}

console.log(correntista.contaBancaria.depositar(1000))
console.log(correntista.nome)
console.log(correntista.contatos) 
console.log(correntista.contaBancaria.saldo) 










