"use strict";
var nome = 'João';
console.log('nome');
let idade = 27;
idade = 27.5;
console.log(idade);
let possuiHobbies = false;
//possuiHobbies = 1;
console.log(possuiHobbies);
let minhaIdade = 'idade 27';
//minhaIdade = 23
console.log(typeof minhaIdade);
let hobbies = ["Conzinhar", "Esportes"];
console.log(hobbies[0]);
console.log(typeof hobbies);
hobbies = [100, 200, 300];
//tuplas
let endereco = ["Avenida Principal", 90, 123];
console.log(endereco);
// enums
var Cor;
(function (Cor) {
    Cor[Cor["Cinza"] = 0] = "Cinza";
    Cor[Cor["Verde"] = 100] = "Verde";
    Cor[Cor["Azul"] = 10] = "Azul";
    Cor[Cor["Laranja"] = 11] = "Laranja";
    Cor[Cor["Amarelo"] = 12] = "Amarelo";
    Cor[Cor["Vermelho"] = 100] = "Vermelho";
})(Cor || (Cor = {}));
let minhaCor = Cor.Verde;
console.log(minhaCor);
console.log(Cor.Azul);
//any
let carro = "BMW";
console.log(carro);
carro = { marca: "BMW", ano: 1976 };
function retornaMeuNome() {
    return nome;
}
console.log(retornaMeuNome());
function digaOi() {
    console.log('Oi');
}
digaOi();
function multiplicar(numA, numB) {
    return numA * numB;
}
console.log(multiplicar(10, 8));
const teste = function teste(numA, numB) {
    return numA + numB;
};
let calculo;
calculo = digaOi;
calculo();
calculo = multiplicar;
console.log(calculo(5, 6));
let funcao3;
funcao3 = multiplicar;
console.log(calculo(5, 6));
let usuario = {
    nome: 'João',
    idade: 27
};
let usuario2 = {
    nome: 'João',
    idade: 27
};
let funcionario = {
    supervisores: ['Ana', 'Fernando'],
    baterPonto(ponto) {
        if (ponto <= 8) {
            return 'Ponto Normal';
        }
        else {
            return 'Fora do horário';
        }
    }
};
let funcionario2 = {
    supervisores: ['Ana', 'Fernando'],
    baterPonto(ponto) {
        if (ponto <= 8) {
            return 'Ponto Normal';
        }
        else {
            return 'Fora do horário';
        }
    }
};
console.log(funcionario.supervisores);
console.log(funcionario.baterPonto(1));
console.log(funcionario.baterPonto(10));
// Unios Types
let nota = 10;
console.log(`Minha nota é ${nota}`);
nota = '10';
console.log(nota);
nota = 'nota  legal';
console.log(nota);
// chacando tipo forma manual
let valor = 30;
if (typeof valor === "number") {
    console.log('É um ');
}
// never ela nunca retorn
function falha(msg) {
    throw new Error(msg);
    //while(true){}
}
const produto = {
    nome: 'Sabao',
    preco: -1,
    validarProduto() {
        if (!this.nome || this.nome.trim().length == 0) {
            falha('Precisa ter um nome');
        }
        if (this.preco <= 0) {
            falha('Preco invalido');
        }
    }
};
// comentado para funcionar o resto
//produto.validarProduto();
//precisa ter um valor valido
let altura = 12;
//altura = null
let alturaOpicional = 12;
alturaOpicional = null;
const contato1 = {
    nome: 'Fulano',
    tel1: '987655432',
    tel2: null
};
console.log(contato1.nome);
console.log(contato1.tel1);
console.log(contato1.tel2);
// vira o tipo any
let podeSerNulo = null;
podeSerNulo = 12;
console.log(podeSerNulo);
let contaBancaria = {
    saldo: 3588,
    depositar(valor) {
        this.saldo += valor;
    }
};
let correntista = {
    nome: 'João da Coves',
    contaBancaria: contaBancaria,
    contatos: ['321321', '321321321']
};
console.log(correntista.contaBancaria.depositar(1000));
console.log(correntista.nome);
console.log(correntista.contatos);
console.log(correntista.contaBancaria.saldo);
//# sourceMappingURL=tipo.js.map