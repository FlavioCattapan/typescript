"use strict";
// usando a variavel anted de init
// usando var imprime undefined 
// usando let imprime 
// ecmascript.ts:2 Uncaught ReferenceError: Cannot access 'seraQuePode' before initialization
//console.log(seraQuePode)
let seraQuePode = '?';
let estaFrio = true;
if (estaFrio) {
    // o var pode ser usado fora do bloco
    var acao = 'Colocar o casaco !';
    // o let não pode ser usado foda do bloco
    // let lacao = 'Colocar o tenis';
}
// não pode ser usado fora do bloco
// console.log(lacao);
console.log(acao);
const cpf = '007.549.966.54';
// tem escopo de bloco
// não pode ser atribuido
//cpf = '008.555.236.21';
function revelar() {
    // let segredo = "interno";
}
for (var i = 0; i < 10; i++) {
    console.log(i);
}
const somar = function (n1, n2) {
    return n1 + n2;
};
console.log(somar(2, 2));
function somar2(n1, n2) {
    return n1 + n2;
}
console.log(somar2(2, 2));
// sem retorno 
const subtrair = (n1, n2) => n1 - n2;
console.log(subtrair(2, 3));
// com retorno
const subtrairComRetorno = (n1, n2) => {
    return n1 - n2;
};
console.log(subtrairComRetorno(6, 3));
// funcao arrow sem parametro
const saudacao = () => console.log("Ola");
console.log(saudacao());
// arow function com parametro se for tipado tem que colocar em parenteses
const falarCom = (pessoa) => console.log('Ola' + pessoa);
falarCom('João');
// this tradicional varia de acordo com quem chamou
//function normalComThis(){
//  console.log(this);
//}
//normalComThis();
//const normalComThisEspecial = normalComThis.bind({nome: 'Ana'});
// normalComThisEspecial();
// arrow function o this nunca varia
// const arrowComThis = () => console.log(this);
// arrowComThis()
// parametro com o valor padrao
function contagemRegressiva(inicio = 6, fim = inicio - 10) {
    console.log(inicio);
    while (inicio > 0) {
        inicio--;
        console.log("Fim");
    }
}
// pode passar o valor ou nao porque ja tem um valor padrao
contagemRegressiva();
contagemRegressiva(5);
// operador spread espalhar ou rest juntar os paremetros
const numbers = [1, 2, 3, 4];
// passando parametro por parametro
console.log(Math.max(numbers[1], numbers[2], numbers[2]));
// spread passa todos os paremtros
console.log(Math.max(...numbers));
const turmaA = ['João', 'Maria'];
const turmaB = ['Marcelo', ...turmaA,
    'Janildo'];
console.log(turmaB);
// retornando um array com javascript
function retornaArray(arg, arg2) {
    return [arg, arg2];
}
// junta os argumentos da função em um array
// tem que ser o primeiro parametro
// não pode colocar parametros dois do ...
function retornaArrayRest(...args) {
    return args;
}
const numeros = retornaArrayRest(1, 3, 5, 7, 4, 9);
console.log(numeros);
// Rest & Sread (Tupla)
const tupla = [1, 'ABC', true];
function tublaParam1(a, b, c) {
    // function somente para demonstrar a chamada
}
tublaParam1(...tupla);
// destructiong pagar dados de uma estrutua e expor de uma forma mais rapida
const caracteristicas = ['Motor 2012', 2020];
const motor = caracteristicas[0];
const ano = caracteristicas[1];
// passa para o array
const [motorD, anoD] = caracteristicas;
console.log(motorD);
console.log(anoD);
const [x, z] = [2, 3];
// desctructiong em relação ao objeto
const item = {
    nome: 'SSD 480gb',
    preco: 200,
    caracteristicas: {
        w: 'Importado'
    }
};
// utilizando emm o operador
const nomeItem = item.nome;
const preco2 = item.preco;
// passando os valores para o objeto 
// dando um apelido nome: n
// da para retirar os atributos aninhados
const { nome: n, preco: p, caracteristicas: { w } } = item;
console.log(n);
console.log(p);
console.log(w);
// template string
const usuarioId = 'SupoertCod3';
const notificacoes = 9;
const boasVindas = 'Boas vindasd' + usuarioId + notificacoes;
const boasVindasTS = `Boas vindas ${usuarioId} ,
Notificacoes: ${notificacoes > 9 ? 'maior ' : 'menor'} more te`;
// Exercicio 1
var dobro = (valor) => {
    return valor * 2;
};
// resposta tirar o retorno
const dobroR = (valor) => valor * 2;
console.log("Resposta exercicio 1 =" + dobro(10));
// Exercicio 2
var dizerOla = function (nome = "Fulano") {
    if (nome === undefined) {
        nome = "Max";
    }
    console.log("Ola, " + nome);
};
const dizerOlaR = (nome = 'Fulano') => { if (nome === undefined)
    nome = ''; };
dizerOlaR();
dizerOlaR("Anna");
// Exercicio 3
const nums = [-3, 33, 38, 5];
// imprimir o menor valor
console.log(Math.min(...nums));
// exercicio 4
const array = [55, 20];
// adiona um array array.push(nums)
array.push(...nums);
// adicionar todos os elementos de "nums" em array
console.log(array);
// exercicio 5
const notas = [8.5, 6.3, 9.4];
const nota1 = notas[0];
const nota2 = notas[1];
const nota3 = notas[2];
const [nota4, nota5, nota6] = notas;
console.log(nota4, nota5, nota6);
// Exercicio 6 Destructing com objeto
const cientista = { primeiroNome: 'Wil', experiencia: 12 };
//const primeiroNome = cientista.primeiroNome
//const experiencia = cientista.experiencia
const { primeiroNome, experiencia } = cientista;
// promisses
// como eu não consigo pegar o retorno eu chamo um callback
function esperar3s(callback) {
    setTimeout(() => {
        console.log('3 s depois... ');
    }, 3000);
}
esperar3s(function (resultado) {
    console.log(resultado);
});
// o promisse tem um mecanismo que chama o callback
function esperar3sPromisse() {
    return new Promise((resolve) => {
        console.log('Chamendo o retorno');
    });
}
esperar3sPromisse().then(dado => console.log(dado));
let urlFilme1;
fetch('https://swapi.co/api/people/1').
    then(res => res.json())
    .then(personagem => personagem.films)
    .then(films => fetch(films[0]))
    .then(resFilm => resFilm.json())
    // .then((filme): string => filme.title)
    .then(function (response) {
    esp3S(response.title);
}).catch(err => console.log(err));
function esp3S(resp) {
    setTimeout(() => {
        console.log(resp);
    }, 3000);
}
console.log(urlFilme1);
//# sourceMappingURL=ecmascript.js.map